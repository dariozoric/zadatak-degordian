<?php

function connect() {
    $conn = new PDO("mysql:host=".HOST.";dbname=".DATABASE,USERNAME,PASSWORD);
    return $conn;
}

function getTwitterPosts($term) {
    $settings = array(
    'oauth_access_token' => TW_TOKEN,
    'oauth_access_token_secret' => TW_TOKEN_SECRET,
    'consumer_key' => TW_KEY,
    'consumer_secret' => TW_SECRET
    );
    
    $url = 'https://api.twitter.com/1.1/search/tweets.json';
    $requestMethod = 'GET';
    $getfield = '?q='.$term.'&result_type=popular';
    
    $twitter = new TwitterAPIExchange($settings);
    
    setAutocomplete($term);
    $string = $twitter->setGetfield($getfield)
        ->buildOauth($url, $requestMethod)
        ->performRequest();
    
    $obj = json_decode($string);
    if(isset($obj->errors[0]->message)) {
        echo "Sorry, there was a problem. Twitter returned the following error message: ".$obj->errors[0]->message;
        exit();
    }
    echo $string;
}

function getTwitterUsers($term) {
    
     $settings = array(
    'oauth_access_token' => TW_TOKEN,
    'oauth_access_token_secret' => TW_TOKEN_SECRET,
    'consumer_key' => TW_KEY,
    'consumer_secret' => TW_SECRET
    );
    
    $url = 'https://api.twitter.com/1.1/users/search.json';
    $requestMethod = 'GET';
    $getfield = '?q='.$term;
    
    $twitter = new TwitterAPIExchange($settings);
    $string = $twitter->setGetfield($getfield)
        ->buildOauth($url, $requestMethod)
        ->performRequest();
    
    $obj = json_decode($string);
    if(isset($obj->errors[0]->message)) {
        echo "Sorry, there was a problem. Twitter returned the following error message: ".$obj->errors[0]->message;
        exit();
    }
    
    echo $string;
}

function getAutocomplete($term) {
    $conn = connect();
    $sql = "SELECT searchText FROM searches WHERE searchText LIKE '%".$term."%' ORDER BY searchDate";
    $q = $conn->prepare($sql);
    $q->execute();
    $row = $q->fetchAll();
    echo json_encode($row);
}

function setAutocomplete($search) {
     //creates account in database triggers when registering
    $conn = connect();
    $sql = "INSERT INTO searches(searchText, searchDate) VALUES(:searchText, NOW())";
    $q = $conn->prepare($sql);
    $q->execute(array( ':searchText' => $search
    ));
   
    return true;
}

?>