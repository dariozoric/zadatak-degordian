searchApp.service('svcSearch', function ($window, $http) {
    
    return({
    searchPosts : searchPosts,
    searchUsers : searchUsers,
    searchSuggestions:searchSuggestions
    });

    function searchPosts(term) {
        var promise = $http({
                      contentType:"application/json",
                      dataType:"json",
                      url:"php/handlers/searchPosts.php?term="+term,          
                      processData:false, 
                      headers: {
                          "Content-Type": "application/json"
                      },
                      method:"GET",
                      error:function(e) { alert('error: '+e);}
                   }).then(function(resp) {
                      
                      return resp.data;
                      
                   });

      
      return promise;      
        
    }
    
    
    function searchUsers(term) {
        var promise = $http({
                      contentType:"application/json",
                      dataType:"json",
                      url:"php/handlers/searchUsers.php?term="+term,          
                      processData:false, 
                      headers: {
                          "Content-Type": "application/json"
                      },
                      method:"GET",
                      error:function(e) { alert('error: '+e);}
                   }).then(function(resp) {
                      
                      return resp.data;
                      
                   });

      
      return promise;      
        
    }
    
    function searchSuggestions(term) {
        var promise = $http({
                      contentType:"application/json",
                      dataType:"json",
                      url:"php/handlers/getSuggestions.php?term="+term,          
                      processData:false, 
                      headers: {
                          "Content-Type": "application/json"
                      },
                      method:"GET",
                      error:function(e) { alert('error: '+e);}
                   }).then(function(resp) {
                      
                      return resp.data;
                      
                   });

      
      return promise;   
    }
    
    
});