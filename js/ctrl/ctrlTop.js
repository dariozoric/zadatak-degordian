searchApp.controller('ctrlTop', function ($scope, $http, $sce, $window, $location, svcSearch) {
    
    $scope.resultsPosts = [];
    $scope.resultsUsers = [];
    $scope.auto = [];
    $scope.pagesUsers = [];
    $scope.pageUsersSelected = 0;
    $scope.pagesPosts = [];
    $scope.pagePostsSelected = 0;
    
    $scope.search = function() {
         svcSearch.searchPosts( encodeURIComponent($scope.searchterm)).then(function(re) {
          $scope.pagePostsSelected = 0;
          if (typeof re == "string") { // if it returns a string we handle errors
            $window.alert(re);
          } else {
          $scope.resultsPosts = re.statuses;
          
          if ($scope.resultsPosts.length != 0) {
            $scope.paginatePosts();
          } else {
            $scope.pagesPosts = [];
          }
          }
        });
         
         svcSearch.searchUsers( encodeURIComponent($scope.searchterm)).then(function(re) {
          $scope.pageUsersSelected = 0;
          if (typeof re == "string") { // if it returns a string we handle errors
            $window.alert(re);
          } else {
            $scope.resultsUsers = re;
            if ($scope.resultsUsers.length != 0) {
              $scope.paginateUsers();
            } else {
              $scope.pagesUsers = [];
            }
          }
        });
    }
    
    $scope.autocomplete = function() {
      svcSearch.searchSuggestions( encodeURIComponent($scope.searchterm)).then(function(re) {
          $scope.auto = re;
          
        });
    }
    
    $scope.paginateUsers = function() {
      $scope.pagesUsers = [];
      pagenumber = Math.ceil($scope.resultsUsers.length/5);
      for(i=0; i<pagenumber; i++) {
        $scope.pagesUsers.push({'text': (i+1), 'start': (i*5), 'active': ''});
      }
      $scope.pagesUsers[0].active = "active";
      
    }
    
     $scope.paginatePosts = function() {
      $scope.pagesPosts = [];
      pagenumber = Math.ceil($scope.resultsPosts.length/5);
      for(i=0; i<pagenumber; i++) {
        $scope.pagesPosts.push({'text': (i+1), 'start': (i*5), 'active': ''});
      }
      $scope.pagesPosts[0].active = "active";
    }
    
    $scope.setUsersSelected = function(val) {
        
       $scope.pageUsersSelected = val;
       pagenumber = Math.ceil($scope.resultsUsers.length/5);
      for(i=0; i<pagenumber; i++) {
        if (i == (val/5)) {
         
          $scope.pagesUsers[i].active = "active";
        } else {
          $scope.pagesUsers[i].active = "";
        }
        
      }
    }
    
     $scope.setPostsSelected = function(val) {
       $scope.pagePostsSelected = val;
       pagenumber = Math.ceil($scope.resultsPosts.length/5);
        for(i=0; i<pagenumber; i++) {
        if (i == (val/5)) {
         
          $scope.pagesPosts[i].active = "active";
        } else {
          $scope.pagesPosts[i].active = "";
        }
        
      }
    }

    $scope.setSearchTerm= function(term) {
      $scope.searchterm = term;
      $scope.search();
    }
    
    $scope.capitalize = function() {
      
    }
    
    $scope.boldenTerm = function(text) {
      // when first letter is upper case
        letter = $scope.searchterm[0].toUpperCase();
        uppercased = $scope.searchterm;
        uppercased = letter+uppercased.split($scope.searchterm[0])[1];
        text= text.replace(uppercased, "<b>"+uppercased+"</b>");
        // when all leters are upper case
        high = '';
        for(i=0; i<uppercased.length; i++) {
          high += (uppercased[i].toUpperCase());
        }
        
        text= text.replace(high, "<b>"+high+"</b>");
        
        //when all is normal
        text= text.replace($scope.searchterm, "<b>"+$scope.searchterm+"</b>");
        
        
        text = $sce.trustAsHtml(text); 
        return text;
    }    
    
    var init = function() {
        
      
         
    }
    
    init();
    
});